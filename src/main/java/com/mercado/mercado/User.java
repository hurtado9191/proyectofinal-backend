/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mercado.mercado;


// import javax.persistence.Entity;
/**
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; */
/**
 *
 * @author juanh
 */

public class User {
    
    //@Id
    //@GeneratedValue(strategy = GenerationType.AUTO)

        private String nombre;
        private String email;
        private String pwd;
        private String token;
        private long tarjeta;
        private String direccion;
        
        public User (){}
        
        public User (String nombre){
            this.nombre = nombre;
            this.token = "";

        }
        public User (String id, String email){
            this.nombre = id;
            this.email = email;
            this.token = "";

        }
           public User (String id, String email, String pwd){
            this.nombre = id;
            this.pwd = pwd;
            this.email= email;
            this.token = "";

        }
        public User (String id, String email,String pwd, String token){
            this.nombre = id;
            this.pwd = pwd;
            this.token= token;
            this.email= email;
            this.token = "";

        }
        public User (String id, String email,String pwd, String direccion, long tarjeta){
            this.nombre = id;
            this.pwd = pwd;
             this.email= email;
             this.tarjeta = tarjeta;
             this.direccion = direccion;
             this.token = "";
        }
        public User (String id, String email,String pwd, String direccion, long tarjeta, String token){
            this.nombre = id;
            this.pwd = pwd;
            this.token= token;
             this.email= email;
             this.tarjeta = tarjeta;
             this.direccion = direccion;
        }
        

	public String getUser() {
		return nombre;
	}

	public void setUser(String id) {
		this.nombre = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
        public void setEmail(String email){
            this.email = email;
        }
        public String getEmail() {
            return email;
        }
        public void setDireccion(String direccion){
            this.direccion = direccion;
        }
        public String getDireccion() {
            return direccion;
        }
        public void setTarjeta(long tarjeta){
            this.tarjeta = tarjeta;
        }
        public long getTarjeta() {
            return tarjeta;
        }
        
        
}
