/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mercado.mercado;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 *
 * @author juanh
 */
@RestController
public class UserController {
    userLista lista = new userLista();
    
        @RequestMapping("/")
        public String index() {
            return "Hello Spring Boot!";
        }

    
        @PostMapping("/user")
	    public User login(@RequestParam("nombre") String nombre, @RequestParam("password") String pwd) {
		String token = getJWTToken(nombre); // TODO change token
                if(lista.gerUserByNameAndPassword(nombre, pwd) == true){
                User user = new User();
		user.setToken(token);
                user.setUser(nombre);
		return user;
                } else return lista.getUser("error");
	}
        
        @PostMapping("/newUser")
        public void newUser(@RequestParam("name") String id,@RequestParam("email") String email,
                @RequestParam("password") String password, @RequestParam("direccion") String direccion, 
                @RequestParam("tarjeta") long tarjeta){
            lista.addUser(id, email, password,direccion,tarjeta);
        }
        @GetMapping("/getUser")
        public User getUser(@RequestParam("name") String id){
            return lista.getUser(id);
        }
        
        @PostMapping("/deleteUser")
        public String deleteUser(@RequestParam("name") String id, @RequestParam("idUser") String user){
            if(user.equals("admin")){
                this.lista.deleteUser(id);
                return "Usuario "+id+" fue eliminado";
            } return "error 418";
        }
        
        private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}


}
