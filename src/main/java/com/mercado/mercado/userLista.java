/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mercado.mercado;

import java.util.*;

/**
 *
 * @author 
 */
public class userLista {
    HashMap<String, User> lista = new HashMap<String,User>();
    User admin = new User("admin","admin@admin.com","admin");
    User pepe = new User("Pepe","pepe@gmail.com","pepe2000");
    User error = new User("error usuario no encontrado");
    public userLista (){
         this.lista.put("admin", admin);
         this.lista.put("pepe", pepe);
         this.lista.put("error ", error);
    }
    
    public void addUser(String id, String email, String password, String direccion, long tarjeta){
        this.lista.put(id, new User(id, email, password, direccion, tarjeta));
    }
    
    public User getUser(String id){
               return this.lista.get(id);
    }
    public void deleteUser(String id){
        this.lista.remove(id);
    }
    
    public boolean gerUserByNameAndPassword(String name, String password){
        User user = this.lista.get(name);
        
        if(user.getPwd().equals(password)){
            return true;
        } else return false;
                     
    }
    
}
