/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mercado.mercado;

/**
 *
 * @author juanh
 */
public class item {
    private String id;
    private String name;
    private double precio;
       
    public item() {}

    public item(String id ){
        this.id = id;
    }

    public item(String id, String name,double precio ){
        this.id = id;
        this.name = name;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(long precio){
        this.precio = precio;
    }


}
