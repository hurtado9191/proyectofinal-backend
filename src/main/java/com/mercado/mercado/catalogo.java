/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mercado.mercado;

import java.util.*;
/**
 *
 * @author juanh
 */
public class catalogo {
        item ff7 = new item("ff7","Final Fantasy VII",150.00);
        item jedi= new item("jedi","Jedi Fallen Order", 350);
        item theLastOfUs = new item("theLastOfUs","The Last Of Us", 412.36);
        item sekiro = new item("sekiro","Sekiro", 654);
        item hp = new item("hp","Harry Potter", 76);
        item junky = new item("junky","Junky", 654.123);
        item haunted = new item("haunted","Haunted", 350.123);
        item oscar = new item("oscar","Oscar", 84.00);
        item tarantino = new item("tarantino","Once Upon A Time In Holywood", 84.00);
        item parasite = new item("parasite","Parasite", 84.00);
        item train = new item("train","Trainspotting", 84.00);
        
        public List<item> vg = new ArrayList<item>();
        public List<item> l = new ArrayList<item>();
        public List<item> p = new ArrayList<item>();
        
        public HashMap<String,item> cat = new HashMap<String, item> ();
        
        
        public catalogo() {
            this.vg.add(ff7);
            this.vg.add(jedi);
            this.vg.add(theLastOfUs);
            
            this.p.add(train);
            this.p.add(tarantino);
            this.p.add(parasite);
            
            this.l.add(hp);
            this.l.add(junky);
            this.l.add(oscar);
            
            this.cat.put("ff7", ff7);
            this.cat.put("jedi", jedi);
            this.cat.put("theLastOfUs", theLastOfUs);
            this.cat.put("sekiro", sekiro);
            this.cat.put("train", train);
            this.cat.put("tarantino", tarantino);
            this.cat.put("parasite", parasite);
            this.cat.put("hp", hp);
            this.cat.put("junky", junky);
            this.cat.put("oscar", oscar);
            this.cat.put("haunted", haunted);
            
        }
        
        public List<item> getVg(){
           
            return vg;
        }
        public List<item> getP(){
           
            return p;
        }
        public List<item> getL(){
            
            return l;
        }
        
        public List<item> getById(String id){
            List<item> err = new ArrayList();
            err.add(new item("400","nada" ,0000));
             switch(id){
                 case "vg": return this.getVg();
                 case "l": return this.getL();
                 case "p": return this.getP();
                 default: return err;
             } 
        }
        public item getItem(String id){
            return cat.get(id);
        }
}
